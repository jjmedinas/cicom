Rails.application.routes.draw do

  resources :presentations
  resources :tickets
  resources :activities
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'health_check#run'
  get 'health-check', to: 'health_check#run', as: :health_check

  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               registration: 'signup'
             },
             controllers: {
               sessions: 'sessions',
               registrations: 'registrations'
             }

  resources :editions

  get 'render-pdf/:file', to: "pdf_files#render_pdf", as: :render_pdf_file
  get 'validate-file/:file/:file_id', to: "validate_files#validate", as: :validate_file

end
