class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.belongs_to :user, null: true
      t.string :name
      t.string :email
      t.string :personal_id
      t.string :company
      t.string :profession

      t.timestamps
    end
  end
end
