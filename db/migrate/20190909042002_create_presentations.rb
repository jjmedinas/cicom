class CreatePresentations < ActiveRecord::Migration[5.2]
  def change
    create_table :presentations do |t|
      t.integer :user_id
      t.integer :judge_id
      t.integer :state
      t.string :title
      t.integer :duration
      t.string :topic
      t.text :comments_from_judge

      t.timestamps
    end
  end
end
