class AddAdditionalAttributesToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :first_name, :string, null: false, default: ''
    add_column :users, :last_name, :string, null: false, default: ''
    add_column :users, :country, :string
    add_column :users, :id_number, :string
    add_column :users, :phone_number, :integer
    add_column :users, :role, :integer
    add_column :users, :presentation, :integer
  end
end
