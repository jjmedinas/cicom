class CreateActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :activities do |t|
      t.string :name
      t.text :description
      t.string :place
      t.datetime :start_time
      t.datetime :end_time
      t.belongs_to :user, null: true
      t.timestamps
    end
  end
end
