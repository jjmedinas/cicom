class AddMissingFieldsToEdition < ActiveRecord::Migration[5.2]
  def change
    add_column :editions, :venue, :string
    add_column :editions, :description, :string
    add_column :editions, :start_datetime, :datetime
    add_column :editions, :end_datetime, :datetime
    add_column :editions, :organizer, :string
    remove_column :editions, :date
  end
end
