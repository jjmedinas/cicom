class AddUsedToTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :used, :boolean, default: false
  end
end
