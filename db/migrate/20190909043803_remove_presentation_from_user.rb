class RemovePresentationFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :presentation
  end
end
