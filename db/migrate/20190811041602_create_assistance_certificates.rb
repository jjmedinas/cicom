class CreateAssistanceCertificates < ActiveRecord::Migration[5.2]
  def change
    create_table :assistance_certificates do |t|
      t.belongs_to :user
      t.json :data
      t.timestamps
    end
  end
end
