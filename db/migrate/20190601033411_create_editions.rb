class CreateEditions < ActiveRecord::Migration[5.2]
  def change
    create_table :editions do |t|
      t.string :name, null: false
      t.date :date, null: false
      t.boolean :is_active, default: false

      t.timestamps
    end
  end
end
