# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#

edition = Edition.where(name: "Cicom 2019", is_active: true, venue: "Cartagena", description: "Cicom 2019 es un evento .....",
               start_datetime: DateTime.parse("2019-10-17 08:00:00 UTC"), end_datetime: DateTime.parse("2019-10-19 08:00:00 UTC"),
               organizer: "Universidad Distrital").first_or_initialize
edition.save

Apartment::Tenant.switch! Edition.get_active

user = User.where(email: "juan@cicom.com", encrypted_password: "123456", first_name: "Juan", last_name: "Perez",
                  country: "Colombia", id_number: "AS34823", phone_number: "553421355", role: "speaker").first_or_initialize
user.password = "123456"
user.password_confirmation = "123456"
user.save

activity = Activity.where(name: "Light talk sobre Microservicios", description: "Talk about...",
                          place: "Salón TIC", start_time: DateTime.parse("2019-06-08T09:00:00.218Z"),
                          end_time: DateTime.parse("2019-06-08T10:00:00.218Z"), user: user).first_or_initialize
activity.save

ticket = Ticket.where(user: user, name: "Juan Perez", email: "juan@perez.com",
                       personal_id: "105235998483", company: "Universidad Distrital / IBM",
                       profession: "Estudiante / Technical Lead").first_or_initialize
ticket.save

diploma = Diploma.where(user: user).first_or_initialize
diploma.data = { presentation_name: 'Agoritmos IA aplicados al mejoramiento del trafico' }
diploma.save

certificate = AssistanceCertificate.where(user: user).first_or_initialize
certificate.save

presentation = Presentation.create(user_id: user.id, judge_id: user.id, state: :sent, title: 'Presentation Title',
                                   duration: 45, topic: "IA", comments_from_judge: "LGTM!")

