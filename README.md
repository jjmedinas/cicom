
# CICOM 2019

Documentación del app movil y web del Congreso Internacional de Computación 2019.

------------

- **Tecnologías**

	- Ruby
	- Ruby on Rails
	- Postgresql
	- Docker

- **Ruby version:**

  `'2.6.3'`

- **Pre-requisitos**
  - docker
  - docker-compose

- **Configuración Desarrollo**

  Primero se crea la imagen de docker y se inicializa la base de datos:

  - `docker-compose build`
  - `docker-compose run --rm web rails db:create`

  A continuación se inicia la consola de rails y se crea un registro de `Edition` con `is_active: true`:

  - `docker-compose run --rm web rails db:create`
  - `Edition.create(name: 'Cicom', date: Date.today, is_active: true)`

  Opcionalmente se puede crear data de prueba en la base de datos ejecutando:

  - `docker-compose run --rm web rails db:seed`

  Por ultimo se inician todos los servicios:

  - `docker-compose up -d`

- **Deployment**

  Para el deployment se debe configurar `docker` y `docker-compose` en el servidor y se debe configurar una base de datos en un servidor diferente. Tambien se deben configurar los permisos adecuados para hacer pull del repositorio con el codigo.

	- `git pull`
	- `cp docker/docker-compose-prod.yml docker-compose.yml`
    - `docker-compose run --rm web rails db:migrate`
    - `docker-compose up -d`


------------

## Documentación de Endpoints

##### POST /signup
```json
{
  "user":
  {
   "email": "5@cicom.com",
   "password": "123456",
   "first_name": "Test 5",
     "last_name": "last name 5",
     "country": "Mexico",
     "id_number": "AS34823",
     "phone_number": "553421355",
     "role": "speaker", #[assistant, speaker, admin, jury, staff]
     "presentation": "How to attend to..." #opcional
  }
}
```

##### POST /login
```json
{
  "user":
  {
   "email": "5@cicom.com",
   "password": "123456",
  }
}
```


##### CRUD Editions - Ej. POST /editions
```json
{
  "edition":
    {
        "id": 99,
        "name": "Cicom 99",
        "is_active": true,
        "venue": "Ruta N",
        "description": "Description 99..... Ruta N...",
        "start_datetime": "2019-06-08T02:39:01.218Z",
        "end_datetime": "2019-06-08T02:39:01.218Z",
        "organizer": "Organizer info"
    }
}
```

##### Agenda - CRUD Activities - Ej. POST /activities
```json
{
  "activity":
    {
        "name": "Light talk sobre Microservicios",
        "description": "Talk about...",
        "place": "Salón TIC",
        "start_time": "2019-06-08T09:00:00.218Z",
        "end_time": "2019-06-08T10:00:00.218Z",
        "user_id": 3
    }
}

```

##### CRUD Tickets - Ej. POST /tickets
```json
{
  "ticket":
    {
        "user_id": 1,
        "name": "Pepe Perez",
        "email": "pepe@perez.com",
        "personal_id": "105235998483",
        "company": "Universidad Distrital / IBM",
        "profession": "Estudiante / Technical Lead"
    }
}

```

##### CRUD Presentations - Ej. POST /presentations

```json
{
  "presentation":
      {
          "user_id": 2,
          "judge_id": 45,
          "state": 0,
          "title": "Titulo charla...",
          "duration": 45,
          "topic": "Tema de la charla (IA, Programación, etc)",
          "comments_from_judge": "Comentarios del jurado"
      }
}
```

State: `{ sent: 0, reviewing: 1, admitted: 2, rejected: 3}`


##### POST /users
```json
{
  "user":
  {
   "email": "5@cicom.com",
   "password": "123456",
   "first_name": "Test 5",
     "last_name": "last name 5",
     "country": "Mexico",
     "id_number": "AS34823",
     "phone_number": "553421355",
     "role": "speaker", #[assistant, speaker, admin, jury, staff]
     "presentation": "How to attend to..." #opcional
  }
}
```

### Archivos PDF y Códigos QR
---
El sistema permite a los usuarios descargar la escarapela de entrada al evento, el certificado de asistencia y el diploma en formato pdf.
El certificado de asistencia y el diploma poseen un código QR que permite a cualquier persona verificar la validez del documento.
Por otra parte, la escarapela de entrada posee un QR que permite a los organizadores del evento, mediante la app, marcar el ingreso del usuario de manera rápida y eficiente.  Al leer el QR de la escarapela  se guarda la fecha y hora exacta de ingreso del usuario y se crea automáticamente el certificado de asistencia.

#### GET /render-pdf/:filename
Permite descargar los distintos tipos de documentos pdf. Las opciones para `:filename` son: `['entrance', 'diploma', 'assistance_certificate']`. Retorna el file correspondiente al current_user, en caso de no existir retorna 404.

#### GET /validate-file/:filename/1
Al leer el código QR que viene en cada uno de los documentos se redirecciona a una url para verificar la validez del archivo. Al igual que en el endpoint de descargar pdf, las opciones para `:filename` son: `['entrance', 'diploma', 'assistance_certificate']`
En caso de ser un archivo válido, se muestra un mensaje de ok y se devuelve un código http 200. En caso de que no sea válido se devuelve un código 404.
Para `'diploma'` y `'assistance_certificate'` no se requiere ningún tipo de autenticación. Para `'entrance'`, que es la escarapela de entrada, se requiere que la persona que lea el código QR haya iniciado sesión con un perfil `staff`.
En caso de que un usuario ya haya mostrado su escarapela al personal organizador del evento, se le haya dado entrada al mismo, y alguien con la misma entrada intente ingresar se mostrará un error diciendo: `"Esta entrada ya fue utilizada hoy."` y se retornará un código http 400.