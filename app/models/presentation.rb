class Presentation < ApplicationRecord
  belongs_to :user
  belongs_to :judge, class_name: 'User', foreign_key: :judge_id

  enum state: { sent: 0, reviewing: 1, admitted: 2, rejected: 3}
end
