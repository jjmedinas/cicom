class Ticket < ApplicationRecord
  belongs_to :user, optional: true

  def download_url
    Rails.application.routes.url_helpers.render_pdf_file(file: "entrance",
                                                         f: self.id,
                                                         u: self.user.id)
  end

  def use!
    used_at_key = "used_at_#{Date.today}"
    if self.used && self.data[used_at_key].present?
      self.errors.add(:used, I18n.t("models.ticket.errors.already_used"))
    else
      self.data[used_at_key] = DateTime.now
      self.used = true
      self.save!
      AssistanceCertificate.create(user: user)
    end
  end

  def qr
    RQRCode::QRCode.new(Rails.application.routes.url_helpers.validate_file_url(file: "entrance",
                                                                               file_id: self.id,
                                                                               host: ENV['HOST']))
  end
end
