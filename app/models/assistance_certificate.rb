class AssistanceCertificate < ApplicationRecord
  belongs_to :user

  def download_url
    Rails.application.routes.url_helpers.render_pdf_file(file: "assistance_certificate",
                                                         f: self.id,
                                                         u: self.user.id)
  end

  def qr
    RQRCode::QRCode.new(Rails.application.routes.url_helpers.validate_file_url(file: "assistance_certificate",
                                                                             file_id: self.id,
                                                                             host: ENV['HOST']))
  end
end
