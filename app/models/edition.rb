class Edition < ApplicationRecord

  validates :name, presence: true
  validates :venue, presence: true
  validates :start_datetime, presence: true
  validates :end_datetime, presence: true
  validate :verify_event_uniqueness

  after_create :create_tenant
  after_destroy :destroy_tenant

  def tenant_name
    "edition_#{self.id}"
  end

  def self.get_active
    where(is_active: true).first.try(:tenant_name)
  end

  private

    def verify_event_uniqueness
      if self.is_active && Edition.where(is_active: true).present?
        self.errors.add(:is_active, I18n.t("models.edition.errors.event_uniqueness"))
      end
    end

    def create_tenant
      Apartment::Tenant.create(self.tenant_name)
    end

    def destroy_tenant
      Apartment::Tenant.drop(self.tenant_name)
    end
end
