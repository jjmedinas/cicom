class Diploma < ApplicationRecord
  belongs_to :user

  def download_url
    Rails.application.routes.url_helpers.render_pdf_file(file: "diploma",
                                                         f: self.id,
                                                         u: self.user.id)
  end

  def qr
    RQRCode::QRCode.new(Rails.application.routes.url_helpers.validate_file_url(file: "diploma",
                                                                           file_id: self.id,
                                                                           host: ENV['HOST']))
  end
end
