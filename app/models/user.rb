class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JWTBlacklist

  validates :email, uniqueness: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  has_many :presentations

  enum role: { assistant: 0, speaker: 1, admin: 2, jury: 3, staff: 4 }

  def full_name
    "#{first_name} #{last_name}"
  end

end
