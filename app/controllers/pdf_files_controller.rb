class PdfFilesController < ApplicationController

  before_action :set_user
  before_action :set_filename
  before_action :set_file

  def render_pdf
    # template_path = "pdf_files/#{Edition.get_active}/#{@filename}"
    template_path = "pdf_files/edition_1/#{@filename}"
    data = send("get_#{@filename}_data")
    pdf_html = ActionController::Base.new.render_to_string(template: template_path, locals: data)
    pdf = WickedPdf.new.pdf_from_string(pdf_html,  orientation: 'Landscape')

    send_data pdf, filename: "#{@filename}-#{data[:user][:first_name]}.pdf"
  end


  private

    def get_entrance_data
      { user: @user,
        file: @file,
        qr: @file.qr
      }
    end

    def get_diploma_data
      { user: @user,
        file: @file,
        qr: @file.qr
      }
    end

    def get_assistance_certificate_data
      { user: @user,
        file: @file,
        qr: @file.qr
      }
    end

    def set_user
      @user = User.find_by id: params[:u] || current_user
      return render json: { error: "User not found" }, status: 400 if @user.nil?
    end

    def set_filename
      valid_options = %w(entrance diploma assistance_certificate)
      @filename = params[:file]

      render json: { error: "Not valid filename" }, status: 400 unless valid_options.include? @filename
    end

    def set_file
      case @filename
      when "entrance"
        @file = Ticket.find_by user_id: current_user ? current_user.id : @user.id
      when "diploma"
        @file = Diploma.find_by user_id: current_user ? current_user.id : @user.id
      when "assistance_certificate"
        @file = AssistanceCertificate.find_by user_id: current_user ? current_user.id : @user.id
      end

      render json: { error: "Not found" }, status: 404 unless @file.present?
    end
end
