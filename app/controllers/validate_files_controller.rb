class ValidateFilesController < ApplicationController

  def validate
    valid_options = %w(entrance diploma assistance_certificate)
    file = params[:file]
    if valid_options.include? file
      send("validate_#{file}")
    else
      render json: { error: "Not valid filename" }, status: 400
    end
  end

  private

    def validate_entrance
      if current_user.try(:role) != 'staff' && current_user.try(:role) != 'admin'
        return render json: { error: "QR para uso interno de CICOM" }, status: 403
      end

      entrance = Ticket.find_by id: params[:file_id]
      user = entrance.user

      if entrance.present? && user.present?
        entrance.use!
        if entrance.errors.empty?
          return render json: { message: "ok" }, status: 200
        else
          return render json: { errors: entrance.errors }, status: 400
        end
      else
        return render json: { error: "No pudimos encontrar el ticket de entrada en nuestro sistema." }, status: 404
      end
    end

    def validate_diploma
      diploma = Diploma.find_by id: params[:file_id]
      user = diploma.user
      if diploma.present? && user.present?
        return render json: { message: "Es un diploma válido.",
                              url: render_pdf_file_url(file: params[:file], u: diploma.user.id,
                                                       f: diploma.id),
                            }, status: 200
      else
        return render json: { error: "No pudimos encontrar el diploma en nuestro sistema." }, status: 404
      end
    end

    def validate_assistance_certificate
      certificate = AssistanceCertificate.find_by id: params[:file_id]
      user = certificate.user
      if certificate.present? && user.present?
        return render json: { message: "Es un certificado de asistencia válido.",
                              url: render_pdf_file_url(file: params[:file], u: certificate.user.id,
                                                       f: certificate.id),
                            }, status: 200
      else
        return render json: { error: "No pudimos encontrar el certificado de asistencia en nuestro sistema." }, status: 404
      end
    end
end
