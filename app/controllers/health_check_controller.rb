class HealthCheckController < ApplicationController

  def run
    return render json: { message: "It Works!" }, status: 200
  end
end
