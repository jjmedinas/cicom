FROM ruby:2.6.3
RUN apt-get update -qq && apt-get install -y build-essential postgresql-client nodejs
RUN mkdir /cicom
WORKDIR /cicom
COPY Gemfile /cicom/Gemfile
COPY Gemfile.lock /cicom/Gemfile.lock
RUN bundle install
COPY . /cicom


# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
